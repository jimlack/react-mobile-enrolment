/*
 * AppConstants
 * These are the variables that determine what our central data store (reducer.js)
 * changes in our state. When you add a new action, you have to add a new constant here
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'YOUR_ACTION_CONSTANT';
 */

export const USERNAME = 'USERNAME';
export const CHALLENGE = 'CHALLENGE';
export const GRANTS = 'GRANTS';
export const CREDENTIALS = 'CREDENTIALS';
export const CHANNEL = 'CHANNEL';
export const SET_AUTH = 'SET_AUTH';
export const SENDING_REQUEST = 'SENDING_REQUEST';
export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';
export const PROFILE = 'PROFILE';

