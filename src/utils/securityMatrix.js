var fetch = require('node-fetch');
var uj = require('url-join');


var securityMatrixUrl = 'http://localhost:4567/securitymatrix/tokenpolicy';


function post(context) {
  return fetch(
    securityMatrixUrl,
    {
      method: 'POST',
      body: JSON.stringify(context),
      headers: {
        'Content-Type': 'application/json',
      }
    }
  )
    .then(function (res) {
      if (res.status < 200 || res.status > 302) {
        // This will handle any errors that aren't network related (network related errors are handled automatically)
        return res.json().then(function (body) {
          console.error('An error occurred while making a HTTP request: ', body);
          return Promise.reject(new Error(body.error.message))
        })
      }
      return res.json();
    })
    .catch(err => console.log(err));
}

var securityMatrix = {
  // find scopes to bake into the token
  getGrants: function (context) {
    return post(context);
  },
  // find minimum required auth strategy
  getAuthStrategy: function (context) {
    return post(context);
  }
}

module.exports = securityMatrix;

