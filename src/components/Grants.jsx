import React from 'react';
import { connect } from 'react-redux';
import {securityMatrixDecision} from "../utils/securityMatrix";
import {changeGrants} from "../actions/AppActions";

const securityMatrix = require('../utils/securityMatrix');



class Grants extends React.Component {

  render() {
    return (
      <div>
        <p>Granting Access agreed by {this.props.data.username} via policy rules</p>
      </div>
    );
  }

  componentDidMount ()
  {
    var dispatch = this.props.dispatch;
    var nav = this.props.history;
    var channel = this.props.data.channel;
    var requestedFunctionality = this.props.data.grants[0];
    // Seems like the user authenticated! Let's tell hydra..
    var attributes = securityMatrix.getGrants({
      "requestedFunctionality": requestedFunctionality,
      "channel": channel,
    }).then(tokenAttributes => {
      dispatch(changeGrants(tokenAttributes.scopes.concat(['openid', 'offline'])));
      nav.push('/scopes');
    });
  }

  /*
  componentDidMount() {
    setTimeout(function () {
      window.location = "leanCustomScheme://message=someLongTokenRXXXXXXegistrationString";
    }.bind(this), 1000);
  }
 */

}

// Which props do we want to inject, given the global state?
function select(state) {
  return {
    data: state
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(Grants);
