var fetch = require('node-fetch');


var profileUrl = 'http://localhost:4569/profiles?userName=';


function post(userName) {
  return fetch(profileUrl + userName)
    .then(function (res) {
      if (res.status < 200 || res.status > 302) {
        // This will handle any errors that aren't network related (network related errors are handled automatically)
        return res.json().then(function (body) {
          console.error('An error occurred while making a HTTP request: ', body);
          return Promise.reject(new Error(body.error.message))
        })
      }
      return res.json();
    })
    .catch(e => {
      console.log(e)
    }
    );
}


var profileService = {
  // message the user with an OTP or a push consent to mobile app
  findProfile: function (context) {
    return post(context);
  }
}

module.exports = profileService;
