// Import React and 3rd Party stuff

import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { homeReducer } from './reducers/reducers';


// Import our home grown
import Username from './components/Username';
import Credentials from './components/Credentials';
import VerificationSuccess from './components/VerificationSuccess';
import Grants from './components/Grants';
import Scopes from './components/Scopes';
import OneTimePassword from './components/OneTimePassword';
import '../scss/app.scss';


require('es6-promise').polyfill();
require('isomorphic-fetch');

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(homeReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());





ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Switch>
          <Route exact path="/login" component={Username} />
          <Route path="/credentials" component={Credentials} />
          <Route path="/otp" component={OneTimePassword} />

          <Route path="/consent" component={VerificationSuccess} />
          <Route path="/grants" component={Grants} />
          <Route path="/scopes" component={Scopes} />
        </Switch>
      </div>
    </Router>
  </Provider>,
  document.getElementById('App')
);
