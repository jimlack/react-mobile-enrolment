import React from 'react';
import { connect } from 'react-redux';
import {changeChallenge, changeUsername, changeGrants, changeChannel} from "../actions/AppActions";

const hydra = require('../utils/hydra');
const queryString = require('query-string');

class VerificationSuccess extends React.Component {

  render() {
    return (
      <div>
        <p>All Done You Verified Sucessfully</p>
      </div>
    );
  }

  componentDidMount ()
  {
    let parsed = queryString.parse(this.props.location.search);
    let challenge = parsed.consent_challenge;
    // stash in redux

    var dispatch = this.props.dispatch;
    dispatch(changeChallenge(challenge));
    var nav = this.props.history;
    hydra.getConsentRequest(challenge)
    // This will be called if the HTTP request was successful
      .then(function (response) {
        // If a user has granted this application the requested scope, hydra will tell us to not show the UI.
        if (response.skip) {
          // You can apply logic here, for example grant another scope, or do whatever...
          // ...

          // Now it's time to grant the consent request. You could also deny the request if something went terribly wrong
          return hydra.acceptConsentRequest(challenge, {
            // We can grant all scopes that have been requested - hydra already checked for us that no additional scopes
            // are requested accidentally.
            grant_scope: response.requested_scope,

            // ORY Hydra checks if requested audiences are allowed by the client, so we can simply echo this.
            grant_access_token_audience: response.requested_access_token_audience,

            // The session allows us to set session data for id and access tokens
            session: {
              // This data will be available when introspecting the token. Try to avoid sensitive information here,
              // unless you limit who can introspect tokens.
              // access_token: { foo: 'bar' },

              // This data will be available in the ID token.
              // id_token: { baz: 'bar' },
            }
          }).then(response => {
            // All we need to do now is to redirect the user back to hydra!
            console.log(response.redirect_to);
          });
        }

        // othwerwise we need to do consent
        dispatch(changeUsername(response.subject));
        dispatch(changeChannel(response.client.client_id));
        dispatch(changeGrants(response.requested_scope));
        nav.push('/grants');

      })
      // This will handle any error that happens when making HTTP calls to hydra
      .catch(error => {
        console.log(error);
      });
  }

  /*
  componentDidMount() {
    setTimeout(function () {
      window.location = "leanCustomScheme://message=someLongTokenRXXXXXXegistrationString";
    }.bind(this), 1000);
  }
 */
}


// Which props do we want to inject, given the global state?
function select(state) {
  return {
    data: state
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(VerificationSuccess);
