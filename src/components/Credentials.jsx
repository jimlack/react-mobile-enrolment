import React from 'react';
import connect from "react-redux/es/connect/connect";
import {changeCredentialData, changeGrants, enrolDevice} from '../actions/AppActions';
import LoadingButton from "./LoadingButton.react";

const assign = Object.assign || require('object.assign');
const hydra = require('../utils/hydra');
const securityMatrix = require('../utils/securityMatrix');


class Credentials extends React.Component {

  render() {
    return (
      <div className="form-page__wrapper">
        <div className="form-page__form-wrapper">
          <div className="form-page__form-header">
            <h2 className="form-page__form-heading">Login 2 of 3 for Username {this.props.data.username} </h2>
            <form className="form" onSubmit={this._auth.bind(this)}>
              <div className="form__field-wrapper">
                <div className="form__field-wrapper">
                  <input className="form__field-input" id="password" onChange={this._changePassword.bind(this)} type="password" placeholder="••••••••••"/>
                  <label className="form__field-label" htmlFor="password">Password</label>
                </div>
              </div>
              <div className="form__field-wrapper">
                <div>
                  <label  className="form__field-pin" htmlFor="pin1">Pin {this.props.data.credentials.pinPos1}</label>
                  <input className="form__field-input" id="pin1" onChange={this._changePin1.bind(this)} type="password" placeholder="•" />
                </div>
                <div>
                  <label className="form__field-pin" htmlFor="pin2">Pin {this.props.data.credentials.pinPos2}</label>
                  <input className="form__field-input" id="pin2" onChange={this._changePin2.bind(this)} type="password" placeholder="•" />
                </div>
              </div>
              <div className="form__submit-btn-wrapper">
                {this.props.currentlySending ? (
                  <LoadingButton/>
                ) : (
                  <button className="form__submit-btn" type="submit">Next</button>
                )}
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

  // Flow to consent
  _auth(evt) {
    evt.preventDefault();
    var nav = this.props.history;
    var channel = this.props.data.channel;
    var requestedFunctionality = this.props.data.grants[0];

    securityMatrix.getAuthStrategy({
      "requestedFunctionality": requestedFunctionality,
      "channel": channel,
    }).then(strategy => {
      if (strategy.authenticationMethod.includes('Two Factor'))
        nav.push('/otp');
      else {
        hydra.acceptLoginRequest(this.props.data.challenge,
          {
            // Subject is an alias for user ID. A subject can be a random string, a UUID, an email address, ....
            subject: 'Jim99',

            // This tells hydra to remember the browser and automatically authenticate the user in future requests. This will
            // set the "skip" parameter in the other route to true on subsequent requests!
            // TODO remember: Boolean(req.body.remember),

            // When the session expires, in seconds. Set this to 0 so it will never expire.
            // TODO remember_for: 3600,

            // Sets which "level" (e.g. 2-factor authentication) of authentication the user has. The value is really arbitrary
            // and optional. In the context of OpenID Connect, a value of 0 indicates the lowest authorization level.
            acr: 'single factor',

          })
          .then(function (response) {
            // All we need to do now is to redirect the user back to hydra!
            window.location = response.redirect_to;
          })
          // This will handle any error that happens when making HTTP calls to hydra
          .catch(function (error) {
            console.log(error);
          });
      }
    });

  }


  // Register a user
  _register(evt) {
    evt.preventDefault();
    this.props.dispatch(enrolDevice(this.props.data));
    this.props.history.push("/success");
  }

  _changePassword(evt) {
    this._emitChange(this._mergeWithCurrentState({
      password: evt.target.value
    }));
  }

  _changePin1(evt) {
    this._emitChange(this._mergeWithCurrentState({
      pin1: evt.target.value
    }));
  }

  _changePin2(evt) {
    this._emitChange(this._mergeWithCurrentState({
      pin2: evt.target.value
    }));
  }

  // Merges the current state with a change
  _mergeWithCurrentState(change) {
    return assign(this.props.data.credentials, change);
  }

  _emitChange(newState) {
    this.props.dispatch(changeCredentialData(newState));

  }
}

// Which props do we want to inject, given the global state?
function select(state) {
  return {
    data: state
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(Credentials);