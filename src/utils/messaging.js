var fetch = require('node-fetch');
var uj = require('url-join');


var messagingUrl = 'http://localhost:4568/mailgateway';


function post(context) {
  return fetch(
    messagingUrl,
    {
      method: 'POST',
      body: JSON.stringify(context),
      headers: {
        'Content-Type': 'application/json',
      }
    }
  )
    .then(function (res) {
      if (res.status < 200 || res.status > 302) {
        // This will handle any errors that aren't network related (network related errors are handled automatically)
        return res.json().then(function (body) {
          console.error('An error occurred while making a HTTP request: ', body);
          return Promise.reject(new Error(body.error.message))
        })
      }
      return res.json();
    });
}

var messagingService = {
  // message the user with an OTP or a push consent to mobile app
  sendMessage: function (context) {
    return post(context);
  }
}

module.exports = messagingService;

