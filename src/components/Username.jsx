import React from 'react';
import {connect} from 'react-redux';
import {changeUsername, changeChallenge, changeChannel, changeGrants, changeProfileData} from '../actions/AppActions';
import LoadingButton from './LoadingButton.react';

const hydra = require('../utils/hydra')
const queryString = require('query-string');
const profileService = require('../utils/profile');

const assign = Object.assign || require('object.assign');

class Username extends React.Component {

  render() {
    return (
      <div className="form-page__wrapper">
        <div className="form-page__form-wrapper">
          <div className="form-page__form-header">
            <h2 className="form-page__form-heading">Login 1 of 3</h2>
            <form className="form" onSubmit={this._submitUsername.bind(this)}>
              <div className="form__field-wrapper">
                <input className="form__field-input" type="text" id="username" placeholder="username"
                  onChange={this._changeUsername.bind(this)} autoCorrect="off" autoCapitalize="off"
                  spellCheck="false"/>
                <label className="form__field-label" htmlFor="username">Username</label>
              </div>
              <div className="form__submit-btn-wrapper">
                {this.props.currentlySending ? (
                  <LoadingButton/>
                ) : (
                  <button className="form__submit-btn" type="submit">Next</button>
                )}
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    let parsed = queryString.parse(this.props.location.search);
    var dispatch = this.props.dispatch;
    let challenge = parsed.login_challenge;
    // stash in redux
    this.props.dispatch(changeChallenge(challenge));
    // let hydra know
    hydra.getLoginRequest(challenge)
      .then(response => {
        console.log('All Good');
        if (response.skip) {
          // Now it's time to grant the login request. You could also deny the request if something went terribly wrong
          // (e.g. your arch-enemy logging in...)
          return hydra.acceptLoginRequest(challenge, {
            // All we need to do is to confirm that we indeed want to log in the user.
            subject: response.subject
          }).then(function (response) {
            // All we need to do now is to redirect the user back to hydra!
            console.log(response.redirect_to);
          });
        }

        dispatch(changeChannel(response.client.client_id));

        // TODO tech debt overloaded requested function for scopes
        dispatch(changeGrants(response.requested_scope));


      });/*.catch(error => {
        console.log(error);
      });*/
  }

  _submitUsername(evt) {
    evt.preventDefault();
    var nav = this.props.history;
    var dispatch = this.props.dispatch;
    profileService.findProfile(this.props.data.userName)
      .then(profile => {
        dispatch(changeProfileData(profile));
        nav.push("/credentials");
      })
      .catch(err => {
        console.log(err);
      })


  }

  _changeUsername(evt) {
    this.props.dispatch(changeUsername(evt.target.value));
  }
}


// Which props do we want to inject, given the global state?
function select(state) {
  return {
    data: state
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(Username);
