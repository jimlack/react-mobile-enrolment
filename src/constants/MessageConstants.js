/*
 * MessageConstants
 * These are the variables that contain the text which is displayed on certain errors
 *
 * Follow this format:
 * export const YOUR_CONSTANT = 'Your message text';
 */
export const FIELD_MISSING = 'Please fill out the entire form.';
export const WRONG_PASSWORD = 'Wrong pinPositions.';
export const USER_NOT_FOUND = 'This password does not exist.';
export const GENERAL_ERROR = 'Something went wrong, please try again';