import React from 'react';
import { connect } from 'react-redux';
import {securityMatrixDecision} from "../utils/securityMatrix";

const hydra = require('../utils/hydra');



class Scopes extends React.Component {

  render() {
    return (
      <div>
        <p>Policy rules applied for OiDC token scopes returning you to Online Banking or Mobile or Open Banking</p>
      </div>
    );
  }

  componentDidMount ()
  {
    var grants = this.props.data.grants;

    var challenge = this.props.data.challenge;
    hydra.getConsentRequest(challenge)
    // This will be called if the HTTP request was successful
      .then(function (response) {
        return hydra.acceptConsentRequest(challenge, {
          // We can grant all scopes that have been requested - hydra already checked for us that no additional scopes
          // are requested accidentally.
          grant_scope: grants,

          // The session allows us to set session data for id and access tokens
          session: {
            // This data will be available when introspecting the token. Try to avoid sensitive information here,
            // unless you limit who can introspect tokens.
            // access_token: { foo: 'bar' },

            // This data will be available in the ID token.
            // id_token: { baz: 'bar' },
          },

          // ORY Hydra checks if requested audiences are allowed by the client, so we can simply echo this.
          grant_access_token_audience: response.requested_access_token_audience,

          // This tells hydra to remember this consent request and allow the same client to request the same
          // scopes from the same user, without showing the UI, in the future.
          //TODO remember: Boolean(, req.body.remember),

          // When this "remember" sesion expires, in seconds. Set this to 0 so it will never expire.
          // TODO remember_for: 3600,
        })
          .then(function (response) {
            // All we need to do now is to redirect the user back to hydra!
            window.location = response.redirect_to;
          })
      })
      // This will handle any error that happens when making HTTP calls to hydra
      .catch(function (error) {
        console.log(error);
      });
  }

  /*
  componentDidMount() {
    setTimeout(function () {
      window.location = "leanCustomScheme://message=someLongTokenRXXXXXXegistrationString";
    }.bind(this), 1000);
  }
 */

}

// Which props do we want to inject, given the global state?
function select(state) {
  return {
    data: state
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(Scopes);
