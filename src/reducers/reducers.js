/*
 * The reducer takes care of our data
 * Using actions, we can change our application state
 * To add a new action, add it to the switch statement in the homeReducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return assign({}, state, {
 *       stateVariable: action.var
 *   });
 */

import {CREDENTIALS, SET_AUTH, SENDING_REQUEST, SET_ERROR_MESSAGE, USERNAME, CHALLENGE, GRANTS, CHANNEL, PROFILE} from '../constants/AppConstants';

// Object.assign is not yet fully supported in all browsers, so we fallback to
// a polyfill
const assign = Object.assign || require('object.assign');


// The initial application state
const initialState = {
  userName : '',
  credentials : {
    password: '',
    pinPos1: 0,
    pin1: 0,
    pinPos2: 3,
    pin2: 0,
    otp:''
  },
  profile: '',
  challenge : '',
  channel : '',
  grants : '',
  currentlySending: false,
  errorMessage: ''
};

// Takes care of changing the application state
export function homeReducer(state = initialState, action) {
  switch (action.type) {
  case USERNAME:
    return assign({}, state, {
      userName: action.newState
    });
    break;
  case CREDENTIALS:
    return assign({}, state, {
      credentials: action.newState
    });
    break;
  case SET_AUTH:
    return assign({}, state, {
      loggedIn: action.newState
    });
    break;
  case SENDING_REQUEST:
    return assign({}, state, {
      currentlySending: action.sending
    });
    break;
  case SET_ERROR_MESSAGE:
    return assign({}, state, {
      errorMessage: action.message
    });

  case CHALLENGE:
    return assign({}, state, {
      challenge: action.newState
    });

  case GRANTS:
    return assign({}, state, {
      grants: action.newState
    });

  case CHANNEL:
    return assign({}, state, {
      channel: action.newState
    });

  case PROFILE:
    return assign({}, state, {
      profile: action.newState
    });


  default:
    return state;
  }
}
